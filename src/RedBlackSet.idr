module RedBlackSet

import Set

%default total

data Color = R | B

export
data RedBlackSet a = E | T Color (RedBlackSet a) a (RedBlackSet a)

balance : Color -> RedBlackSet a -> a -> RedBlackSet a -> RedBlackSet a
balance B (T R (T R e x b) y c) z d = T R (T B e x b) y (T B c z d)
balance B (T R e x (T R b y c)) z d = T R (T B e x b) y (T B c z d)
balance B e x (T R (T R b y c) z d) = T R (T B e x b) y (T B c z d)
balance B e x (T R b y (T R c z d)) = T R (T B e x b) y (T B c z d)
balance color e x b = T color e x b

export
Ord a => Set RedBlackSet a where
  empty = E

  member x E = False
  member x (T _ l y r) = case compare x y of
    LT => member x l
    GT => member x r
    EQ => True

  insert x s = assert_total $ let T _ l y r = ins s in T B l y r
   where
    -- ins always returns a T (never an E)
    ins : RedBlackSet a -> RedBlackSet a
    ins E = T R E x E
    ins s'@(T color l y r) = case compare x y of
      LT => balance color (ins l) y r
      GT => balance color l y (ins r)
      EQ => s'
