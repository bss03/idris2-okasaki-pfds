module Trie

import Data.Maybe

import FiniteMap

%default total

export
partial
data Trie : (Type -> Type) -> Type -> Type -> Type where
  MkTrie : Maybe a -> mk (Trie mk ks a) -> Trie mk ks a

export
partial
{m : Type -> Type -> Type} -> {k : Type} ->
FiniteMap m k => FiniteMap (Trie (m k)) (List k) where
  empty = MkTrie Nothing empty

  lookup [] t = case t of
    MkTrie b m => b
  lookup (k :: ks) t = case t of
    MkTrie b m => lookup k m >>= \m' => lookup ks m'

  bind [] x t = case t of
    MkTrie b m => MkTrie (Just x) m
  bind (k :: ks) x t = case t of
    MkTrie b m => let t = fromMaybe empty $ lookup k m
                      t' = bind ks x t
                  in MkTrie b (bind k t' m)
