module BootstrapHeap

import Heap

%default total

export
partial
data BootstrapHeap : (Type -> Type) -> Type -> Type where
  E : BootstrapHeap h a
  H : a -> h (BootstrapHeap h a) -> BootstrapHeap h a

partial
Eq a => Eq (BootstrapHeap h a) where
  x == y = case x of
    (H x' _) => case y of
      (H y' _) => x' == y'

partial
Ord a => Ord (BootstrapHeap h a) where
  compare x y = case x of
    (H x' _) => case y of
      (H y' _) => compare x' y'

export
partial
{h : Type -> Type} -> Heap h => Heap (BootstrapHeap h) where
  empty = E
  isEmpty E = True
  isEmpty _ = False

  insert x h = merge (H x empty) h

  merge E h2 = h2
  merge h1 E = h1
  merge h1@(H x p1) h2@(H y p2) =
    if x <= y then H x (insert h2 p1) else H y (insert h1 p2)

  findMin h = case h of
    E       => idris_crash "empty heap"
    (H x p) => x

  deleteMin E = idris_crash "empty heap"
  deleteMin (H x p) =
      if isEmpty p then E
      else let (H y p1) = findMin p
               p2 = deleteMin p
           in (H y (merge p1 p2))
