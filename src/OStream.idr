module OStream -- If just "Stream", irreconsilable conflict with names from Prelude.Stream

%default total

mutual
  public export
  data StreamCell a = Nil | (::) a (OStream a)

  public export
  infixr 7 ::

  {-
  I haven't figured out how define Stream at the top-level and not conflict with the version from
  Prelude.
  -}
  public export
  OStream : Type -> Type
  OStream a = Lazy (StreamCell a)

mutual
  append' : OStream a -> OStream a -> OStream a
  append' []       t = t
  append' (x :: s) t = x :: append s t

  export
  append : OStream a -> OStream a -> OStream a
  append s t = Delay (Force (append' s t))

  export
  (++) : OStream a -> OStream a -> OStream a
  (++) = append

mutual
  take' : Int -> OStream a -> OStream a
  take' 0 s        = []
  take' n []       = []
  take' n (x :: s) = x :: take (n - 1) s

  export
  take : Int -> OStream a -> OStream a
  take n s = Delay (Force (take' n s))

drop' : Int -> OStream a -> OStream a
drop' 0 s        = s
drop' n []       = []
drop' n (x :: s) = drop' (n - 1) s

export
drop : Int -> OStream a -> OStream a
drop n s = Delay (Force (drop' n s))

reverse' : OStream a -> OStream a -> OStream a
reverse' []       r = r
reverse' (x :: s) r = reverse' s (x :: r)

export
reverse : OStream a -> OStream a
reverse s = Delay (Force (reverse' s []))
