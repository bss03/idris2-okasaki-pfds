module TrieOfTrees

import Data.Maybe

import FiniteMap

%default total

export
data Tree a = E | T a (Tree a) (Tree a)

export
partial
data Trie : (Type -> Type) -> Type -> Type -> Type where
  MkTrie : Maybe a -> mk (Trie mk ks (Trie mk ks a)) -> Trie mk ks a

export
partial
{m : Type -> Type -> Type} -> {k : Type} ->
FiniteMap m k => FiniteMap (Trie (m k)) (Tree k) where
  empty = MkTrie Nothing empty

  lookup E t = case t of
    MkTrie v mk => v
  lookup (T k a b) t = case t of
    MkTrie v mk => do
      m' <- lookup k mk
      m'' <- lookup a m'
      lookup b m''

  bind E x t = case t of
    MkTrie v mk => MkTrie (Just x) mk
  bind (T k a b) x t = case t of
    MkTrie v mk => let tt  = fromMaybe empty $ lookup k mk
                       t   = fromMaybe empty $ lookup a tt
                       t'  = bind b x t
                       tt' = bind a t' tt
                   in MkTrie v (bind k tt' mk)
