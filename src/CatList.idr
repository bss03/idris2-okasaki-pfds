module CatList

import CatenableList
import Queue

%default total

export
partial
data CatList : (Type -> Type) -> Type -> Type where
  E : CatList q a
  C : {q : Type -> Type} -> a -> q (Lazy (CatList q a)) -> CatList q a

partial
link : Queue q => CatList q a -> Lazy (CatList q a) -> CatList q a
link c s = case c of
  (C x xs) => C x (snoc xs s)
  E => idris_crash "link: to E"

partial
linkAll : {q : Type -> Type} -> Queue q => CatList q a -> q (Lazy (CatList q a)) -> CatList q a
linkAll t q' =
  if isEmpty q'
   then t
   else link t (linkAll (force $ Queue.head q') (assert_smaller q' $ tail q'))

export
partial
{q : Type -> Type} -> Queue q => CatenableList (CatList q) where
  empty = E
  isEmpty E = True
  isEmpty _ = False

  xs ++ E  = xs
  E  ++ ys = ys
  xs ++ ys = link xs ys

  cons x xs = (C x empty) ++ xs
  snoc xs x = xs ++ (C x empty)

  head E = idris_crash "empty list"
  head (C x _) = x

  tail E = idris_crash "empty list"
  tail (C x xs) =
    if isEmpty xs then E else linkAll (force $ Queue.head xs) (tail xs)
