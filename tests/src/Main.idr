module Main

import Hedgehog

import QueueTest
import DequeTest

allTests : List Group
allTests = queueTests :: dequeTests :: Nil

main : IO ()
main = test allTests
