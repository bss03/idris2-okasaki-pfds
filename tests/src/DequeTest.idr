module DequeTest

import Data.List
import Data.Vect

import Hedgehog

import Deque
import BankersDeque

data DAct = Cons | Tail | Snoc | Init

Show DAct where
  showPrec _ qa = case qa of
    Cons => "Cons"
    Tail => "Tail"
    Snoc => "Snoc"
    Init => "Init"

actions' : Nat -> Nat -> Gen (List DAct)
actions' dSize addCount = case dSize of
  Z => case addCount of
    Z => pure Nil
    (S pCount) => do
      act <- element (Cons :: Snoc :: Nil)
      more <- actions' (S dSize) pCount
      pure (act :: more)
  (S pSize) => case addCount of
    Z => do
      act <- element (Tail :: Init :: Nil)
      more <- actions' pSize addCount
      pure (act :: more)
    (S pCount) => do
      act <- frequency
        (  (addCount, pure Cons)
        :: (dSize, pure Tail)
        :: (addCount, pure Snoc)
        :: (dSize, pure Init)
        :: Nil
        )
      let
        grow = actions' (S dSize) pCount
        wilt = actions' pSize addCount
      more <- case act of
        Cons => grow
        Tail => wilt
        Snoc => grow
        Init => wilt
      pure (act :: more)

actions : Gen (List DAct)
actions = sized (\sz => actions' 0 sz.size)

partial
Deque List where
  empty = Nil
  isEmpty = isNil

  cons = (::)
  head l = head l {ok = idris_crash "head"}
  tail l = tail l {ok = idris_crash "tail"}

  snoc l x = foldl (flip (::)) (x :: Nil) l
  last l = last l {ok = idris_crash "last"}
  init l = init l {ok = idris_crash "init"}

elem : PropertyT Int
elem = forAll (int (constant 0 100))

propDeque : (d : Type -> Type) -> Deque d => Property
propDeque d = property (forAll actions >>= loop empty Deque.empty)
 where
   loop : d Int -> List Int -> List DAct -> PropertyT ()
   loop d ld as = do
     case isEmpty d of
      True => assert (isEmpty ld)
      False => do
        False === isEmpty ld
        head d === head ld
        last d === last ld
     case as of
      Nil => pure ()
      (Cons :: t) => do
        e <- elem
        loop (cons e d) (cons e ld) t
      (Tail :: t) => loop (tail d) (Deque.tail ld) t
      (Snoc :: t) => do
        e <- elem
        loop (snoc d e) (snoc ld e) t
      (Init :: t) => loop (init d) (Deque.init ld) t

export
dequeTests : Group
dequeTests = MkGroup
  { name = "Deque implementation Tests"
  , properties = [( "Deque BankersDeque", propDeque BankersDeque)]
  }
