module QueueTest

import Data.List
import Data.Vect

import Hedgehog

import Queue
import BankersQueue
import BatchedQueue
import BootstrappedQueue
import HoodMelvilleQueue
import ImplicitQueue
import PhysicistsQueue

data QAct = Snoc | Tail

Show QAct where
  showPrec _ qa = case qa of
                   Snoc => "Snoc"
                   Tail => "Tail"

actions' : Nat -> Nat -> Gen (List QAct)
actions' Z Z = pure Nil
actions' Z (S p) = do
    more <- actions' (S Z) p
    pure (Snoc :: more)
actions' (S p) Z = do
    more <- actions' p Z
    pure (Tail :: more)
actions' size@(S pSize) need@(S pNeed) = do
    next <- frequency ((size, pure Tail) :: (need, pure Snoc) :: Nil)
    more <- case next of
     Tail => actions' pSize need
     Snoc => actions' (S size) pNeed
    pure (next :: more)

actions : Gen (List QAct)
actions = sized (\sz => actions' 0 sz.size)

partial
Queue List where
  empty = Nil
  isEmpty = isNil

  head l = head l {ok = idris_crash "head"}
  tail l = tail l {ok = idris_crash "tail"}
  snoc l x = foldl (flip (::)) (x :: Nil) l

elem : PropertyT Int
elem = forAll (int (constant 0 100))

propQueue : (q : Type -> Type) -> Queue q => Property
propQueue q = property (forAll actions >>= loop empty Queue.empty)
 where
   loop : q Int -> List Int -> List QAct -> PropertyT ()
   loop q lq as = do
     case isEmpty q of
      True => assert (isEmpty lq)
      False => do
        False === isEmpty lq
        head q === head lq
     case as of
      Nil => pure ()
      (Snoc :: t) => do
        e <- elem
        loop (snoc q e) (snoc lq e) t
      (Tail :: t) => loop (tail q) (Queue.tail lq) t

export
queueTests : Group
queueTests = MkGroup
  { name = "Queue implementation Tests"
  , properties =
      [ ("Queue BankersQueue", propQueue BankersQueue)
      , ("Queue BatchedQueue", propQueue BatchedQueue)
      , ("Queue BootstrappedQueue", propQueue BootstrappedQueue)
      , ("Queue HoodMelvilleQueue", propQueue HoodMelvilleQueue)
      , ("Queue ImplicitQueue", propQueue ImplicitQueue)
      , ("Queue PhysicistsQueue", propQueue PhysicistsQueue)
      ]
  }
